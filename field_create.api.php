<?php

/**
 * @file
 * Contains hook examples for this module.
 */

/**
 * Implements hook_field_create_definitions().
 */
function hook_field_create_definitions(array &$definitions) {
  // Define your fields here by entity type.
  $definitions['node'] = [
    'demo_field' => [
      'name'    => 'demo_field',
      'label'   => 'This is my field',
      'type'    => 'string',
      // No update once field exists.
      'force'   => FALSE,
      'bundles' => [
        'page' => [
          'label'    => 'Oulala',
          'displays' => [
            'form' => [
              'default' => [
                // Force display.
                'region' => 'content',
              ],
            ],
            'view' => [
              'default' => [
                'label' => 'above',
              ],
              'teaser'  => [
                'label' => 'hidden',
              ],
            ],
          ],
        ],
      ],
    ],
  ];
}
